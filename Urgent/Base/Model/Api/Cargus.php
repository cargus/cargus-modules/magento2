<?php
/**
 * Created by PHPStorm
 * User: Alexandru Marinescu
 * Date: 31.03.2022
 * Copyright: Tremend Software Consulting
 */
declare(strict_types=1);

namespace Urgent\Base\Model\Api;

use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Urgent\Base\Api\Data\TokenInterface;
use Urgent\Base\Api\Data\TokenInterfaceFactory;
use Urgent\Base\Api\TokenRepositoryInterface;
use Urgent\Base\Logger\Logger;
use Urgent\Base\Model\ResourceModel\Token\Collection as TokenCollection;
use Laminas\Http\Response;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\HTTP\ZendClientFactory;
use Urgent\Base\Model\Config\Config;
use Zend_Http_Client;
use Zend_Http_Client_Exception;
use Zend_Http_Response;

/**
 * Class Cargus
 *
 * With this class you can make requests api to cargus.
 */
abstract class Cargus
{
    private const LOGIN = 'LoginUser';

    /** @var Logger $_logger */
    protected Logger $_logger;
    /** @var Config $_config */
    protected Config $_config;
    /** @var ZendClientFactory $_zendClientFactory */
    protected ZendClientFactory $_zendClientFactory;
    /** @var TokenCollection $_tokenCollection */
    private TokenCollection $_tokenCollection;
    /** @var TimezoneInterface $_timezone */
    protected TimezoneInterface $_timezone;
    /** @var TokenInterfaceFactory $_tokenFactory */
    private TokenInterfaceFactory $_tokenFactory;
    /** @var TokenRepositoryInterface $_tokenRepository */
    private TokenRepositoryInterface $_tokenRepository;
    /** @var SerializerInterface $_serializer */
    protected SerializerInterface $_serializer;
    /** @var DirectoryList $_directoryList */
    protected DirectoryList $_directoryList;
    /** @var EncryptorInterface $_encryptor */
    protected EncryptorInterface $_encryptor;

    /**
     * Constructor
     *
     * @param Logger $logger
     * @param Config $config
     * @param ZendClientFactory $zendClientFactory
     * @param TokenCollection $tokenCollection
     * @param TimezoneInterface $timezone
     * @param TokenInterfaceFactory $tokenFactory
     * @param TokenRepositoryInterface $tokenRepository
     * @param SerializerInterface $serializer
     * @param DirectoryList $directoryList
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        Logger                   $logger,
        Config                   $config,
        ZendClientFactory        $zendClientFactory,
        TokenCollection          $tokenCollection,
        TimezoneInterface        $timezone,
        TokenInterfaceFactory    $tokenFactory,
        TokenRepositoryInterface $tokenRepository,
        SerializerInterface      $serializer,
        DirectoryList            $directoryList,
        EncryptorInterface       $encryptor
    ) {
        $this->_logger = $logger;
        $this->_config = $config;
        $this->_zendClientFactory = $zendClientFactory;
        $this->_tokenCollection = $tokenCollection;
        $this->_timezone = $timezone;
        $this->_tokenFactory = $tokenFactory;
        $this->_tokenRepository = $tokenRepository;
        $this->_serializer = $serializer;
        $this->_directoryList = $directoryList;
        $this->_encryptor = $encryptor;
    }

    /**
     * Method getClient
     *
     * @param string $method
     *
     * @return ZendClient
     */
    protected function getClient(string $method = Zend_Http_Client::GET): ZendClient
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Ocp-Apim-Subscription-Key' => $this->_config->getApiKey(),
            'Path' => $this->getVersionMagento()
        ];
        $client = $this->_zendClientFactory->create();
        try {
            $client->setConfig([
                'timeout' => $this->_config->getApiTimeout(),
            ]);
            $client->setHeaders($headers);
            $client->setMethod($method);
        } catch (Zend_Http_Client_Exception $e) {
            if ($this->_config->getDebugLogger()) {
                $this->_logger->critical($e->getMessage());
            }
        }
        return $client;
    }

    /**
     * Method login
     *
     * @param bool $forceToken
     *
     * @return string
     * @throws CouldNotSaveException
     */
    protected function login(bool $forceToken = false): string
    {
        $currentDate = $this->_timezone->date();
        if (!$forceToken) {
            $token = $this->_tokenCollection
                ->setOrder('id')
                ->setPageSize(1);
            if ($token->getSize()) {
                $token = $token->getFirstItem();
                /** @var TokenInterface $token */
                $tokenDate = $this->_timezone->date($token->getAvailable());
                if (!$currentDate->diff($tokenDate)->invert) {
                    return $token->getToken();
                }
            }
        }

        /** @var ZendClient $client */
        $client = $this->getClient(Zend_Http_Client::POST);
        try {
            $client->setUri($this->_config->getApiUrl() . self::LOGIN);
            $client->setParameterPost([
                'UserName' => $this->_config->getApiUsername(),
                'Password' => $this->_encryptor->decrypt($this->_config->getApiPassword()),
            ]);
            $request = $this->doRequest($client);
            if ($request['success']) {
                $token = $this->_tokenFactory->create();
                $token->setToken(trim($request["body"], '"'));

                $token->setAvailable($currentDate->modify('+20 hours')->format('Y-m-d H:i:s'));
                $this->_tokenRepository->save($token);
                return $token->getToken();
            }
        } catch (Zend_Http_Client_Exception $e) {
            if ($this->_config->getDebugLogger()) {
                $this->_logger->critical($e->getMessage());
            }
        }
        return '';
    }

    /**
     * Method doRequest
     *
     * @param ZendClient $client
     *
     * @return array
     */
    protected function doRequest(ZendClient $client): array
    {
        $result = [
            'success' => true,
            'body' => ''
        ];

        if (!$this->_config->getApiIsActive()) {
            $result['success'] = false;
            $result['body'] = __('The urgent cargus api is disabled!');
            return $result;
        }

        try {
            $response = $client->request();
            if ($this->_config->getDebugLogger()) {
                $this->_logger->info($client->getUri(true));
            }
            $this->checkResponse($response, $result);
        } catch (Zend_Http_Client_Exception $e) {
            if ($this->_config->getDebugLogger()) {
                $this->_logger->critical($e->getMessage());
            }
            $result['success'] = false;
            $result['body'] = __('Something went wrong: ') . '(' . $e->getMessage() . ')';
        }

        return $result;
    }

    /**
     * Method checkResponse
     *
     * @param Zend_Http_Response $response
     * @param array $result
     *
     * @return void
     * @throws Zend_Http_Client_Exception
     */
    private function checkResponse(Zend_Http_Response $response, array &$result): void
    {
        if ($response->getStatus() === Response::STATUS_CODE_200) {
            $result['body'] = $response->getBody();
            if ($this->_config->getDebugLogger()) {
                $this->_logger->info($result['body']);
            }
        } else {
            $throwMsg = 'Request Status: ' . $response->getStatus();
            $throwMsg .= ' Body: ' . $response->getBody();
            throw new Zend_Http_Client_Exception($throwMsg);
        }
    }

    /**
     * Method getVersionMagento
     *
     * @return string
     */
    private function getVersionMagento(): string
    {
        // Version must have maxim 5 characters.
        $rootDir = $this->_directoryList->getRoot();
        $filePath = $rootDir . DIRECTORY_SEPARATOR . 'composer.json';
        // phpcs:disable Magento2.Functions.DiscouragedFunction.Discouraged
        if (file_exists($filePath)) {
            $composer = $this->_serializer->unserialize(file_get_contents($filePath));
            // phpcs:enable Magento2.Functions.DiscouragedFunction.Discouraged
            if (isset($composer['version'])) {
                return 'M' . substr(str_replace(['.', '-'], '', trim($composer['version'])), 0, 4);
            }
        }
        return 'MNVF'; //Magento no version found
    }

    /**
     * Method execute
     *
     * @return mixed
     */
    abstract public function execute();
}

<?php
/**
 * Created by PHPStorm
 * User: Alexandru Marinescu
 * Date: 19.05.2022
 * Copyright: Tremend Software Consulting
 */
declare(strict_types=1);

namespace Urgent\Base\Logger;

/**
 * Class Logger
 *
 * Description class.
 */
class Logger extends \Monolog\Logger
{
}
